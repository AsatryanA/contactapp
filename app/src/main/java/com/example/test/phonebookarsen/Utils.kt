package com.example.test.phonebookarsen

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

const val BASE_URL = "https://stdevtask3-0510.restdb.io/rest/"
const val api_key = "a5b39dedacbffd95e1421020dae7c8b5ac3cc"


fun Fragment.addTo(activity: AppCompatActivity, container: Int) {
    val transaction = activity.supportFragmentManager.beginTransaction()
    transaction.add(container, this)
    transaction.addToBackStack(null)
    transaction.commit()
}


fun Fragment.replace(activity: AppCompatActivity, container: Int) {
    val transaction = activity.supportFragmentManager.beginTransaction()
    transaction.replace(container, this)
    transaction.addToBackStack(null)
    transaction.commit()
}