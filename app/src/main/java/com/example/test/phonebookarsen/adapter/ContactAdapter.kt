package com.example.test.phonebookarsen.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.phonebookarsen.R
import com.example.test.phonebookarsen.model.Contact
import kotlinx.android.synthetic.main.contact_item.view.*

class ContactAdapter(private val contacts: ArrayList<Contact>) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    var mListener: OnContactItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false))

    override fun getItemCount() = contacts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]
        holder.name.text = contact.firstName
        holder.number.text = contact.phone
        holder.itemView.setOnClickListener {
            mListener?.onContactItemClickListener(contact)
        }

        holder.remove.setOnClickListener {
            mListener?.removeContact(contact._id)
            removeItem(contact)

        }
        holder.edit.setOnClickListener {
            mListener?.editContactData(contact)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.contactImage!!
        val name = itemView.ContactName!!
        val number = itemView.contactNumber!!
        val remove = itemView.remove!!
        val edit = itemView.edit!!
    }


    private fun removeItem(contact: Contact) {
        contacts.remove(contact)
        notifyDataSetChanged()
    }

    interface OnContactItemClickListener {
        fun onContactItemClickListener(contact: Contact)

        fun removeContact(id: String)

        fun editContactData(contact: Contact)

    }
}