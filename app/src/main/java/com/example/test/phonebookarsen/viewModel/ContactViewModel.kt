package com.example.test.phonebookarsen.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.test.phonebookarsen.model.Contact
import com.example.test.phonebookarsen.repository.ContactRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ContactViewModel(application: Application) : AndroidViewModel(application) {

    private val contactRepository: ContactRepository = ContactRepository
    val clickedUser = MutableLiveData<Contact>()


    fun getContactListAsync(callback: (ArrayList<Contact>) -> Unit) {
        GlobalScope.launch(Dispatchers.Main) {
            callback(contactRepository.getContactList())
        }
    }

    fun addNewContact(contact: Contact) {
        contactRepository.addNewUser(contact)
    }

    fun removeContactFromList(id: String) {
        contactRepository.removeContact(id)
    }

    fun editContact(id: String, contact: Contact) {
        contactRepository.editContact(id, contact)
    }
}