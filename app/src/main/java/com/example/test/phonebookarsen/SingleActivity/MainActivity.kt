package com.example.test.phonebookarsen.SingleActivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.test.phonebookarsen.R
import com.example.test.phonebookarsen.fragment.ContactListFragment
import com.example.test.phonebookarsen.replace

class MainActivity : AppCompatActivity() {



    private lateinit var contactListFragment: ContactListFragment


    private fun initContents() {

        contactListFragment = ContactListFragment.newInstance()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initContents()
        contactListFragment.replace(this, R.id.container)

    }
}
