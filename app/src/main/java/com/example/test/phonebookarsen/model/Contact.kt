package com.example.test.phonebookarsen.model

data class Contact(
        val _id: String,
        val firstName: String,
        val lastName: String,
        val phone: String,
        val email: String,
        val notes: String,
        val images: ArrayList<String>?
)
