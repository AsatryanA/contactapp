package com.example.test.phonebookarsen.repository

import com.example.test.phonebookarsen.api.ApisService
import com.example.test.phonebookarsen.api.RetrofitClient
import com.example.test.phonebookarsen.model.Contact
import kotlinx.coroutines.*
import retrofit2.HttpException

object ContactRepository {
    private var service: ApisService = RetrofitClient.getClient()
            .create(ApisService::class.java)

    suspend fun getContactList(): ArrayList<Contact> {
        val contactList = ArrayList<Contact>()
        withContext(Dispatchers.IO) {
            try {
                val response = service.getContactsListAsync().await()
                contactList.addAll(response)
            } catch (e: HttpException) {
                e.message()
            }

        }
        return contactList
    }

    fun addNewUser(c: Contact) {
        runBlocking {
            try {
                service.addNewContactAsync(c.firstName, c.lastName, c.phone, c.email, c.notes, c.images)
            } catch (e: HttpException) {
                e.response()
            }

        }
    }

    fun removeContact(id: String) {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                service.removeContactFromListAsync(id).await()
            } catch (e: HttpException) {

            }
        }
    }

    fun editContact(id: String, c: Contact) {
        runBlocking {
            try {
                service.editContactAsync(id, c.firstName, c.lastName, c.phone, c.email, c.notes, c.images).await()
            } catch (e: HttpException) {
                e.response()
            }
        }
    }
}