package com.example.test.phonebookarsen.api

import com.example.test.phonebookarsen.model.Contact
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.http.*

interface ApisService {


    @GET("contacts")
    fun getContactsListAsync(): Deferred<ArrayList<Contact>>

    @POST("contacts")
    @FormUrlEncoded
    fun addNewContactAsync(
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("phone") phone: String,
            @Field("email") email: String,
            @Field("notes") notes: String,
            @Field("images") images: ArrayList<String>?
    ): Deferred<ResponseBody>

    @DELETE("contacts/{objectid}")
    fun removeContactFromListAsync(@Path("objectid") id: String): Deferred<ResponseBody>

    @PUT("contacts/{objectid}")
    @FormUrlEncoded
    fun editContactAsync(@Path("objectid") id: String,
                         @Field("firstName") firstName: String,
                         @Field("lastName") lastName: String,
                         @Field("phone") phone: String,
                         @Field("email") email: String,
                         @Field("notes") notes: String,
                         @Field("images") images: ArrayList<String>?): Deferred<ResponseBody>
}