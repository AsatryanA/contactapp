package com.example.test.phonebookarsen.api

import com.example.test.phonebookarsen.BASE_URL
import com.example.test.phonebookarsen.api_key
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    private var retrofit: Retrofit? = null

    fun getClient(): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .client(getOkHttpClient())
                    .build()
        }

        return retrofit!!
    }

    private fun getOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .readTimeout(1000, TimeUnit.SECONDS)
                .connectTimeout(1000, TimeUnit.SECONDS)
                .addInterceptor {
                    val request = it.request().newBuilder()
                            .addHeader("accespt", "application/json")
                            .addHeader("x-apikey", api_key)
                            .build()

                    return@addInterceptor it.proceed(request)
                }
                .build()
    }
}