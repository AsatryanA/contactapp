package com.example.test.phonebookarsen.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.phonebookarsen.R
import com.example.test.phonebookarsen.viewModel.ContactViewModel
import kotlinx.android.synthetic.main.user_page.*

class ContactPageFragment : Fragment() {

    private lateinit var viewModel: ContactViewModel

    companion object {
        fun newInstance(): ContactPageFragment {
            return ContactPageFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.user_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(activity!!).get(ContactViewModel::class.java)
        drawContact()
    }

    private fun drawContact() {
        viewModel.clickedUser.observe(this, Observer {
            if (it != null) {
                firstName.text = it.firstName
                lastName.text = it.lastName
                phone.text = it.phone
                email.text = it.email
                notes.text = it.notes

            }
        })
    }

}