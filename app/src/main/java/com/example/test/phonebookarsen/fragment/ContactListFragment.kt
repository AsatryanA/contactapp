package com.example.test.phonebookarsen.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.test.phonebookarsen.R
import com.example.test.phonebookarsen.SingleActivity.MainActivity
import com.example.test.phonebookarsen.adapter.ContactAdapter
import com.example.test.phonebookarsen.model.Contact
import com.example.test.phonebookarsen.replace
import com.example.test.phonebookarsen.viewModel.ContactViewModel
import kotlinx.android.synthetic.main.contact_list.*

class ContactListFragment : Fragment(), ContactAdapter.OnContactItemClickListener {


    private lateinit var viewModel: ContactViewModel
    private lateinit var contactPageFragment: ContactPageFragment
    private lateinit var newContactFragment: AddNewContactFragment
    private lateinit var editContactFragment: EditContactFragment

    companion object {
        fun newInstance(): ContactListFragment {
            return ContactListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contact_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(activity!!).get(ContactViewModel::class.java)
        initContents()
        drawRecyclerView()
        add.setOnClickListener {
            val act = activity as MainActivity
            newContactFragment.replace(act, R.id.container)
        }
    }

    private fun initContents() {
        contactPageFragment = ContactPageFragment.newInstance()
        newContactFragment = AddNewContactFragment.newInstance()
        editContactFragment = EditContactFragment.newInstance()
    }

    private fun drawRecyclerView() {
        contactRecyclerView.layoutManager = LinearLayoutManager(activity)
        contactRecyclerView.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        viewModel.getContactListAsync {
            val adapter = ContactAdapter(it)
            adapter.mListener = this
            contactRecyclerView.adapter = adapter

        }
    }

    override fun onContactItemClickListener(contact: Contact) {
        val act = activity as MainActivity
        viewModel.clickedUser.postValue(contact)
        contactPageFragment.replace(act, R.id.container)
    }

    override fun removeContact(id: String) {
        viewModel.removeContactFromList(id)
    }

    override fun editContactData(contact: Contact) {
        val act = activity as MainActivity
        viewModel.clickedUser.postValue(contact)
        editContactFragment.replace(act, R.id.container)
    }

}