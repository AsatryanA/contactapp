package com.example.test.phonebookarsen.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.test.phonebookarsen.R
import com.example.test.phonebookarsen.model.Contact
import com.example.test.phonebookarsen.viewModel.ContactViewModel
import kotlinx.android.synthetic.main.edit_contact.*

class EditContactFragment : Fragment() {

    private lateinit var viewModel: ContactViewModel

    companion object {
        fun newInstance(): EditContactFragment {
            return EditContactFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.edit_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(activity!!).get(ContactViewModel::class.java)
        drawContact()
    }

    private fun drawContact() {
        viewModel.clickedUser.observe(this, Observer {
            if (it != null) {
                firstName.setText(it.firstName)
                lastName.setText(it.lastName)
                phone.setText(it.phone)
                email.setText(it.email)
                notes.setText(it.notes)

            }

            editContact.setOnClickListener { view ->
                editContact(it?._id!!)
            }
        })
    }

    private fun editContact(contact_id: String) {
        var i = 0
        val id = phone.text.toString()
        val name = firstName.text.toString()
        if (name.isNotEmpty()) {
            i++
        }
        val surname = lastName.text.toString()
        if (surname.isNotEmpty()) {
            i++
        }
        val phone = phone.text.toString()
        if (phone.isNotEmpty()) {
            i++
        }
        val email = email.text.toString()
        if (email.isNotEmpty()) {
            i++
        }
        val notes = notes.text.toString()
        if (notes.isNotEmpty()) {
            i++
        }
        if (i >= 5) {
            val editContact = Contact(id, name, surname, phone, email, notes, null)
            viewModel.editContact(contact_id, editContact)
            activity!!.supportFragmentManager.popBackStack()
        } else {
            editContact.isEnabled = false
            Toast.makeText(activity, "Please Enter Fields", Toast.LENGTH_SHORT).show()
        }

    }

}